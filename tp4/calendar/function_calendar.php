<?php
//FONCTION PRINCIPALE DU CALENDRIER
function calendar($m_give,$y_give){

    $month = array();
    $month[1] = "Janvier";
    $month[2] = "Février";
    $month[3] = "Mars";
    $month[4] = "Avril";
    $month[5] = "Mai";
    $month[6] = "Juin";
    $month[7] = "Juillet";
    $month[8] = "Août";
    $month[9] = "Septembre";
    $month[10] = "Octobre";
    $month[11] = "Novembre";
    $month[12] = "Décembre";

    
    if ( isset($_GET['m'])){
        $m = $_GET['m'];
    }
    else {
        $m = $m_give; 
    }
    if ( isset($_GET['a'])){
         $y = $_GET['a'];
    }
    else {
        $y = $y_give;    
    }
   
    $bissextil = bissextil($y);
    
    if ($bissextil){
        $nbrday = array(0,31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    }
    else{
        $nbrday = array(0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
    }

    $firstofmonth = jddayofweek(cal_to_jd( CAL_GREGORIAN, $m, 1, $y), 0);
    
    if($firstofmonth == 0){
        $firstofmonth = 7;
    }
    
    $ini_array = parse_ini_file("../secret.ini", true);
    try {
       $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
       $database = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);      
    } catch (Exception $e) {
    exit('Erreur de connexion à la base de données.'.$e->getMessage());
    }
    $information = $database->prepare('Select name,startdate,enddate,organizer_id,id,nb_place From events  ');
    $information ->execute();
    $information = $information->fetchAll();
    
    echo '<table>'
          . ' <tr>'
            . " <td class=\"arrow\">" . month_before($m,$y).'</td>'
            . "<td class=\"name_month\" colspan=5>". htmlspecialchars ($month[$m]) .' '. htmlspecialchars ($y) .'</td>'
            . " <td class=\"arrow\">" .month_after($m,$y). '</td>'
            . '</tr>'
                . "<tr class=\"name_day\">"
                    .'<td> Lundi</td>'
                    . '<td>Mardi </td>'
                    . '<td>Mercredi</td>'
                    . '<td>Jeudi</td>'
                    . '<td>Vendredi</td>'
                    . '<td>Samedi</td>'
                    . '<td>Dimanche</td>'
                . '</tr>'   
            . '<tr>';

if ( isset( $_SESSION['rank_user'] ) != FALSE && $_SESSION['rank_user'] == 'ORGANIZER'){
    
        $day=1;   
        for($i=1;$i<40;$i++){        
            if($i < $firstofmonth){    
                echo "<td class=\"empty_cases\"></td>";
                }
            else{
                if($day == date("d") && $m == date("n")){    
                    echo "<td class=\"day today\">".htmlspecialchars ($day).event_today_organizer($y , $m ,$day,$information).'</td>';
                }
                
                else{ 
                    echo "<td class=\"day\">".htmlspecialchars ($day).event_today_organizer($y , $m ,$day,$information).'</td>';
                }
                $day++;   

                if($day > ($nbrday[$m])){
                    while($i % 7 != 0){
                        echo "<td class=\"empty_cases\"></td>";
                        $i++;
                    }
                echo '</tr>'
                    . '</table>';
                $i=41;
                }
            }

            if($i % 7 == 0){
                echo '</tr>'
                . '<tr>';
            }

        }
    }
    else {
        $day=1;   
        for($i=1;$i<40;$i++){        
            if($i < $firstofmonth){    
                echo "<td class=\"empty_cases\"></td>";
            }
            else{
                if($day == date("d") && $m == date("m")){    
                    echo "<td class=\"day today\">".htmlspecialchars ( $day) .   event_today_default($y , $m ,$day,$information).' </td>';
                }
                else{ 
                    echo "<td class=\"day\">".htmlspecialchars ($day) . event_today_default($y , $m ,$day,$information).'</td>';
                }
                $day++;   

                if($day > ($nbrday[$m])){
                    while($i % 7 != 0){
                        echo "<td class=\"empty_cases\"></td>";
                        $i++;
                    }
                echo '</tr>'
                    . '</table>';
                $i=41;
                }
            }

            if($i % 7 == 0){
                echo '</tr>'
                . '<tr>';
            }

        }
    }

}

function month_after($m,$y){
    $m++;    
    if($m==13){   
        $y++;
        $m=1;
    }
    return "<a href=/calendar/template.php?m=".htmlspecialchars($m)."&y=".htmlspecialchars($y)." > &gt;&gt;  </a>";
}

function month_before($m,$y){
    $m--;
    if($m==0){
        $y--;
        $m=12;
    }
    return "<a href=/calendar/template.php?m=".htmlspecialchars($m)."&y=".htmlspecialchars($y)."> &lt;&lt;  </a>";
}


function event_today_default($y , $m ,$day,$information){
    $date2 = new DateTime($y.'-'.$m.'-'.$day);
    $one_time =0;
    $more_5_event = 0;
    $events="";
    foreach($information as $result_database_event){
      $id_e = $result_database_event['id'];
      $start = $result_database_event['startdate'];
      $start = datetime::createFromFormat("Y-m-d H:i:s", $start);
      $end = $result_database_event['enddate'];
      $end = datetime::createFromFormat("Y-m-d H:i:s", $end);
         
    if (isset($_SESSION['id'])){
       
        $result_database_if_user_participate = database_user_participates_events($_SESSION['id'] , $id_e);
        if ($date2 <= $end && $date2 >= $start && isset($result_database_if_user_participate['id_event'])){ 
            $one_time++;
            $more_5_event++;

            if ($one_time ==1){
                $events = "<div class=\"top_event\" >  Evénements </div> <div class=\"event\">  ";
            }
            if ($more_5_event > 5){
                $events.= "<button onclick=\"self.location.href='/see_more/see_more.php?m=".htmlspecialchars($m)."&y=".htmlspecialchars($y)."&d=".htmlspecialchars($day)."'\" class=\"see_more\" > Voir plus </button>";
                break; 
            }
            $events .= "<a href=/reservation_event/reservation_event.php?&id_e=".htmlspecialchars($id_e)." class=\"name_event reserve\">".htmlspecialchars ($result_database_event['name']).'</a> <br>' ;
        }
        else if($date2 <= $end && $date2 >= $start){
            $one_time++;
            $more_5_event++;

            if ($one_time ==1){
                $events = "<div class=\"top_event\" >  Evénements </div> <div class=\"event\">  ";
            }
            if ($more_5_event > 5){
                $events.= "<button onclick=\"self.location.href='/see_more/see_more.php?m=".htmlspecialchars($m)."&y=".htmlspecialchars($y)."&d=".htmlspecialchars($day)."'\" class=\"see_more\" > Voir plus </button>";
                break; 
            }
                $events .= "<a href=/reservation_event/reservation_event.php?&id_e=$id_e class=\"name_event\">".htmlspecialchars ($result_database_event['name']).'</a> <br>' ;
        }
    }

    else {
        if ($date2 <= $end && $date2 >= $start ){ 
            $one_time++;
            $more_5_event++;

            if ($one_time ==1){
                $events = "<div class=\"top_event\" >  Evénements </div> <div class=\"event\">  ";
            }
            if ($more_5_event > 5){
                $events.= "<button onclick=\"self.location.href='/see_more/see_more.php?m=".htmlspecialchars($m)."&y=".htmlspecialchars($y)."&d=".htmlspecialchars($day)."'\" class=\"see_more\" > Voir plus </button>";
                break; 
            }
            $events .= "<a href=/reservation_event/reservation_event.php?&id_e=".htmlspecialchars($id_e)." class=\"name_event\">".htmlspecialchars ($result_database_event['name']).'</a> <br>' ;
        }
        
    }
         
       
    }
    
    if ($one_time >0){
        $events .= "</div>";
    }
    return $events;
}

function database_user_participates_events($id , $id_event){
     $ini_array = parse_ini_file("../secret.ini", true);
     
    try {
       $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
       $database = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);      
    } catch (Exception $e) {
    exit('Erreur de connexion à la base de données.'.$e->getMessage().$ini_array['db']['dsn']);
    }
    $information = $database->prepare('Select  id_event From user_participates_events Where id_participant = ? AND id_event=?');
    $information ->execute(array($id , $id_event));
    return $information->fetch();
}

function event_today_organizer($y , $m ,$day,$information){
    $date2 = new DateTime($y.'-'.$m.'-'.$day);
    $one_time =0;
    $more_5_event = 0;
    $events="";
    foreach($information as $result){
      $id_e = $result['id'];
      $start = $result['startdate'];
      $start = datetime::createFromFormat("Y-m-d H:i:s", $start);
      $end = $result['enddate'];
      $end = datetime::createFromFormat("Y-m-d H:i:s", $end);
      if ($date2 <= $end && $date2 >= $start && $_SESSION['id'] == $result['organizer_id']){
          $one_time++;
          $more_5_event++;

          if ($one_time ==1){
              $events = "<div class=\"top_event\" >  Evénements </div> <div class=\"event\">  ";
          }
           if ($more_5_event > 5){
             $events.= "<button  onclick=\"self.location.href='/see_more/see_more.php?m=".htmlspecialchars($m)."&y=".htmlspecialchars($y)."&d=".htmlspecialchars($day)."'\" class=\"see_more\" > Voir plus </button>";
             break; 
       }
        $events .= "<a href=/delete_event/delete_event.php?id_e=".htmlspecialchars($id_e)." class=\"name_event\">".htmlspecialchars ($result['name']).'</a> <br>' ;
       
      }
    }
    
    if ($one_time >0){
        $events .= "</div>";
    }
    return $events;
}


function bissextil($y) {
	if( ($y/4 && !$y/100) || $y/400) {
		return TRUE;
	} 
        else {
		return FALSE;
	}
}

?>


