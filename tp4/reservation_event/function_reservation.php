<?php

function database_user_participates_events(){
     $ini_array = parse_ini_file("../secret.ini", true);
    try {
       $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
       $database = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);      
    } catch (Exception $e) {
    exit('Erreur de connexion à la base de données.'.$e->getMessage());
    }
    $information = $database->prepare('Select id_participant , id_event From user_participates_events  ');
    $information ->execute();
    return $information;
}


function section($id){
    $information = database_events($id);
    $result = $information->fetch();
    $startdate =  new DateTime($result['startdate']);
    $startdate_y_m_d = date_format($startdate,'Y-m-d');
    $enddate=  new DateTime($result['enddate']);
    $enddate_y_m_d =  date_format($enddate,'Y-m-d');
    echo '<p> <span class="before_information_event">Nom : </span>'.htmlspecialchars($result['name']).'</p>';
    echo '<p> <span class="before_information_event"> Description : </span>'.htmlspecialchars($result['description']).'</p>';
    echo '<p> <span class="before_information_event"> Nombre de places restantes : </span>'.htmlspecialchars($result['nb_place']).'</p>';
    echo '<p> <span class="before_information_event"> Date de commencement : </span>'.htmlspecialchars(date($startdate_y_m_d)).'</p>';
    echo '<p> <span class="before_information_event"> Date de fin : </span>'.htmlspecialchars(date($enddate_y_m_d)).'</p>';
    
    
    
    
    
}



function database_verif_good_organizer(){
    $ini_array = parse_ini_file("../secret.ini", true);
    try {
       $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
       $database = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);      
    } catch (Exception $e) {
    exit('Erreur de connexion à la base de données.'.$e->getMessage());
    }
    $information = $database->prepare('Select organizer_id From events Where organizer_id=? ');
    $information ->execute(array($_GET['id_e']));
    $result = $information->fetch();
    return $result['id_organizer'];
}

function database_events($id){
     $ini_array = parse_ini_file("../secret.ini", true);
    try {
       $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
       $database = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);      
    } catch (Exception $e) {
    exit('Erreur de connexion à la base de données.'.$e->getMessage());
    }
    $information = $database->prepare('Select name , startdate , enddate , description ,nb_place From events Where id=? ');
    $information ->execute(array($id));
    return $information;
}

function verification_space($id){
   $information= database_events($id);
   $result=$information->fetch();
   if($result['nb_place']>0){
       return 1;
   }
   return 0;
}



function title($id){
    $information = database_events($id);
    $result =$information->fetch();;
    $title = $result['name'];
   return  htmlspecialchars($title);
}


function verif_reservation(){
    $information = database_user_participates_events();
    foreach($information as $result){
     if ($result['id_participant'] == $_SESSION['id'] && $result['id_event'] == $_GET['id_e']){
         return 0;
     }

    }
    return 1;
    
 
}