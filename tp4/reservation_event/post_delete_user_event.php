<?php
session_start();
if ( !isset($_GET['id_e'])){
    echo'Aucun événement selectionné';
    echo' <a href="/calendar/template.php">  Menu principal ?</a>';
}
else{
    delete_reservation_event();
    space_add();
    $_SESSION['success']=1;
    header('Location: /calendar/template.php?');
    exit();
}
function verif_nb_place(){
    $ini_array = parse_ini_file("../secret.ini", true);
    try {
       $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
       $database = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);      
    } catch (Exception $e) {
    exit('Erreur de connexion à la base de données.'.$e->getMessage());
    }
    $information = $database->prepare('Select nb_place From events Where id=:id ');
    $information->execute(array('id' => $_GET['id_e'] ));
    $information = $information->fetch();
    return $information['nb_place'];
}


function space_add(){
     $ini_array = parse_ini_file("../secret.ini", true);
    try {
       $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
       $database = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);   


    } catch (Exception $e) {
            exit('Erreur de connexion à la base de données.'.$e->getMessage());
    }
    $req = $database->prepare('UPDATE  events SET nb_place=:place WHERE id= :id ');    
    $req->execute(array('id' => $_GET['id_e'],'place' => verif_nb_place()+1 ));
    
    
    
}

function delete_reservation_event(){
     $ini_array = parse_ini_file("../secret.ini", true);
    try {
       $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
       $database = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);   


    } catch (Exception $e) {
            exit('Erreur de connexion à la base de données.'.$e->getMessage());
    }
    $req = $database->prepare('DELETE FROM user_participates_events WHERE id_participant= :id_participant AND id_event= :id_event');    
    $req->execute(array('id_participant' => $_SESSION['id'],'id_event' => $_GET['id_e'] ));
    
    
    
}