<?php
session_start();
if(!isset($_POST['Name_username'])){
 header('Location: /register/register.php');
 exit();
}
if(!isset($_POST['Name_password'])){
 header('Location: /register/register.php');
 exit();
}
$ini_array = parse_ini_file("../secret.ini", true);
try {
   $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
   $database = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);   

   
$pass_hache = password_hash($_POST['Name_password'], PASSWORD_DEFAULT);
$req = $database->prepare('INSERT INTO Users(login, password) VALUES(:login, :password)');
$req->execute(array('login' => $_POST['Name_username'],'password' => $pass_hache ));
} catch (Exception $e) {
        exit('Erreur de connexion à la base de données.'.$e->getMessage());
}



 header('Location: /calendar/template.php');
 exit();

