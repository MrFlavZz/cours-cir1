<?php
session_start();

$error = 0;
if (!isset($_POST['Name_password'])){
    $error = 1;
    include('./sign_in.php');
    exit();
}


$ini_array = parse_ini_file("../secret.ini", true);
try {
   $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
   $database = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);   

} catch (Exception $e) {
        exit('Erreur de connexion à la base de données.'.$e->getMessage());
}

$data = $database->prepare('Select password ,id,rank From Users  Where login = ?');
$data->execute(array($_POST["Name_username"]));
$result = $data->fetch();
$isPasswordCorrect = password_verify($_POST['Name_password'], $result['password']);

if($isPasswordCorrect){
    $_SESSION['user'] =$_POST["Name_username"];
    $_SESSION['id'] = $result['id'];
    $_SESSION['rank_user'] = $result['rank'];
    header('Location: /calendar/template.php');
    exit();
}
else{
    $error = 1;
    include('./sign_in.php');
    exit();
}
