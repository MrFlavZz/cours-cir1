<?php
session_start();
    
   
if ( !isset( $_SESSION['id'] ) ){
    echo'Veuillez vous connecter';
    echo' <a href="/sign_in/sign_in.php">  Se connecter ?</a>';
}
else if ($_SESSION['rank_user'] == 'ORGANIZER' ){
    include('./view_create_event.php');
    exit();
}
else if ($_SESSION['rank_user'] == 'CUSTOMER' ){
    echo'En tant que customer vous ne pouvez pas creer un évènement';
    echo' <a href="/calendar/template.php">  Menu principal </a>';
}
