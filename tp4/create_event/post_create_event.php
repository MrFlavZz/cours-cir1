<?php
session_start();    



if ( !isset( $_SESSION['id'] ) ){
    echo'Veuillez vous connecter';
    echo' <a href="/sign_in/sign_in.php">  Se connecter ?</a>';
}
else if ( $_SESSION['rank_user'] != 'ORGANIZER' ){
    echo"Vous ne pouvez pas creer d'événement";
    echo' <a href="/calendar/template.php">  Menu principal </a>';
}
else {
    create_event();
}

function create_event(){
    $name_event = $_POST['name_event'];
    $nb_place = $_POST['nb_place'];
    $startdate =  $_POST['startdate'];
    $enddate = $_POST['enddate'];
    $description = $_POST['description'];
    $error_type = -1;
 
    if (!isset($name_event) ||!isset($nb_place) || !isset($startdate) || !isset($enddate)|| !isset($description)){
        $error_type = 0;
        include('./create_event.php');
        exit();
    }
     if (strlen($name_event) >20){
        $error_type = 1;
        include('./create_event.php');
        exit();
    }
     if (strlen($description)>500){
        $error_type = 2;
        include('./create_event.php');
        exit();
    }
    if (!is_numeric($nb_place)){
        $error_type = 3;
        include('./create_event.php');
        exit();
    }

    if ($nb_place>200){
        $error_type = 4;
        include('./create_event.php');
        exit();
    }

    if ($startdate> $enddate){
         $error_type = 5;
        include('./create_event.php');
        exit();
    }
    if ($nb_place<=0){
        $error_type = 6;
        include('./create_event.php');
        exit();
    }

     $ini_array = parse_ini_file("../secret.ini", true);
    try {
       $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
       $database = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);      
       $information = $database->prepare('INSERT INTO events(name,description,startdate,enddate,organizer_id,nb_place) VALUES(:name_event,:descripion,:startdate,:enddate,:organizer_id,:nb_place)');
   
    } catch (Exception $e) {
    exit('Erreur de connexion à la base de données.'.$e->getMessage());
    }
     $information ->execute(array('name_event' => $name_event ,'descripion' => $description , 'startdate' => "$startdate",'enddate' => "$enddate",'organizer_id' => $_SESSION['id'] ,'nb_place' =>$nb_place));
    $_SESSION['success'] = 3;
    header('Location: /calendar/template.php');
    exit();

}



