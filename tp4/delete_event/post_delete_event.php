<?php

session_start();
if ( !isset($_GET['id_e'])){
    echo'Aucun événement selectionné';
    echo' <a href="/calendar/template.php">  Menu principal ?</a>';
}

else{
    delete_all_customer_in_event();
    delete_event();
    $_SESSION['success']=4;
    header('Location: /calendar/template.php?');
    exit();
}

function delete_all_customer_in_event(){
     $ini_array = parse_ini_file("../secret.ini", true);
    try {
       $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
       $database = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);   


    } catch (Exception $e) {
            exit('Erreur de connexion à la base de données.'.$e->getMessage());
    }
    $req = $database->prepare('DELETE FROM user_participates_events WHERE id_event= :id_event');    
    $req->execute(array('id_event' => $_GET['id_e'] ));
    
}

function delete_event(){
     $ini_array = parse_ini_file("../secret.ini", true);
    try {
       $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
       $database = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);   


    } catch (Exception $e) {
            exit('Erreur de connexion à la base de données.'.$e->getMessage());
    }
    $req = $database->prepare('DELETE FROM events WHERE id= :id_event');    
    $req->execute(array('id_event' => $_GET['id_e'] ));
    
}


