<?php

session_start();
include ('function_delete_event.php');

if ( !isset( $_SESSION['id'] ) ){
    echo'Veuillez vous connecter';
    echo' <a href="./sign_in/sign_in.php">  Se connecter ?</a>';
}
else if ( !isset( $_SESSION['id'] ) != database_verif_good_organizer()){
    echo'Vous ne pouvez pas supprimer cette événement';
    echo' <a href="/calendar/calendar.php">  Menu principal ?</a>';
}
else if ($_SESSION['rank_user'] == 'CUSTOMER' ){
    echo"Vous ne pouvez pas creer d'événement";
    echo' <a href="/calendar/calendar.php">  Menu principal ?</a>';
}
else if ($_SESSION['rank_user'] == 'ORGANIZER' ){
     include('view_delete_event.php');
    exit();
}
