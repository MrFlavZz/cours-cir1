<?php
function database_user_participates_events($id , $id_event){
     $ini_array = parse_ini_file("../secret.ini", true);
    try {
       $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
       $database = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);      
    } catch (Exception $e) {
    exit('Erreur de connexion à la base de données.'.$e->getMessage());
    }
    $information = $database->prepare('Select  id_event From user_participates_events Where id_participant = ? AND id_event=?');
    $information ->execute(array($id , $id_event));
    return $information->fetch();
}


function database_events(){
 $ini_array = parse_ini_file("../secret.ini", true);
    try {
       $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
       $database = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);      
    } catch (Exception $e) {
    exit('Erreur de connexion à la base de données.'.$e->getMessage());
    }
    $information = $database->prepare('Select name,startdate,enddate,organizer_id,id,nb_place From events  ');
    $information ->execute();
    $information = $information->fetchAll();
    return $information;
}

function display_all_event_organizer($d , $y ,$m){
    $information = database_events();
    $date2 = new DateTime($y.'-'.$m.'-'.$d);
    foreach($information as $result_database_event){
      
      $id_e = $result_database_event['id'];
      $space = $result_database_event['nb_place'];
      $start = $result_database_event['startdate'];
      $start = datetime::createFromFormat("Y-m-d H:i:s", $start);
      $end = $result_database_event['enddate'];
      $end = datetime::createFromFormat("Y-m-d H:i:s", $end);
    
     
      if (isset($_SESSION['id'])){
        
        if ($date2 <= $end && $date2 >= $start && $result_database_event['organizer_id'] == $_SESSION['id']){ 
        echo "<a href=/delete_event/delete_event.php?id_e=$id_e class=\"name_event\">".htmlspecialchars ($result_database_event['name']).'</a> <br>' ;
        }

      }
      
      else {
        echo'Erreur';
      }
    }
   
}


function display_all_event_customer($d , $y ,$m){
    $information = database_events();
    $date2 = new DateTime($y.'-'.$m.'-'.$d);
    foreach($information as $result_database_event){
      
      $id_e = $result_database_event['id'];
      $space = $result_database_event['nb_place'];
      $start = $result_database_event['startdate'];
      $start = datetime::createFromFormat("Y-m-d H:i:s", $start);
      $end = $result_database_event['enddate'];
      $end = datetime::createFromFormat("Y-m-d H:i:s", $end);
    

      if (isset($_SESSION['id'])){
       $result_database_if_user_participate = database_user_participates_events($_SESSION['id'] , $id_e);
        if ($date2 <= $end && $date2 >= $start && isset($result_database_if_user_participate['id_event'])){ 
            echo "<a href=/reservation_event/reservation_event.php?&id_e=".htmlspecialchars($id_e)." class=\"name_event reserve\">".htmlspecialchars ($result_database_event['name']).'</a> <br>' ;
        }
        else if($date2 <= $end && $date2 >= $start && $space>0){
            echo "<a href=/reservation_event/reservation_event.php?&id_e=".htmlspecialchars($id_e)." class=\"name_event\">".htmlspecialchars ($result_database_event['name']).'</a> <br>' ;

        }
      }
      
      else {
        echo'Erreur';
      }
    }
   
}

function title($d , $y ,$m){
    $month = array();
    $month[1] = "Janvier";
    $month[2] = "Février";
    $month[3] = "Mars";
    $month[4] = "Avril";
    $month[5] = "Mai";
    $month[6] = "Juin";
    $month[7] = "Juillet";
    $month[8] = "Août";
    $month[9] = "Septembre";
    $month[10] = "Octobre";
    $month[11] = "Novembre";
    $month[12] = "Décembre";
    
    $result = $d .' '. $month[$m].' ' . $y;
    return htmlspecialchars($result);
    
}