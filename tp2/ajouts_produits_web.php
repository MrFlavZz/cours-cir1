<!DOCTYPE html>
<html lang="fr">
  <head>
   
      <link rel="stylesheet" href="CSS.php" type="text/css" media="all" />
    <meta charset="utf-8">
    <title> Ajout produit </title>
  </head>
  <body>
  <div class="Option" id="Option">
      <form action="ajouts_produits.php" method="post" enctype="multipart/form-data">

        <h1> Ajouter un produit </h1>
        <p id="P_Number_product">Nombre de produit : <input type="number"  placeholder="Ex : 20"  name="Name_number_product" required> </p>
         <p id="P_Price_product">Prix du produit / unité : <input type="number" step="0.10" placeholder="Ex : 10 euros"  name="Name_price_product" required> </p>
         <p id="P_Name_product">Nom : <input type="text" placeholder="Ex : Lait"  name="Name_name_product" required> </p>
         <p id="P_File_product">Photo : <input type="file"  name="Name_name_file" required> </p>
        <p id="P_Submit_product"><input type="submit" name="Envoie" value="OK" class="CLASS_submit"></p>
      </form>
      <a href="listes_produits_web.php"> Voir la liste des produits </a>
      <?php
      
if (isset($_POST['Name_number_product'])){
    $i =  $_SESSION['product_number'];
    $extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png' );
    $info = strtolower(  substr(  strrchr($_FILES['Name_name_file']['name'], '.')  ,1)  );   
    if (isset($_FILES['Name_name_file']['name']) && isset($_SESSION['Name_number_product_'.$i]) && isset($_SESSION['Name_price_product_'.$i])){
        if ($_SESSION['Name_number_product_'.$i] < 0 || $_SESSION['Name_price_product_'.$i] < 0 || (isset($_SESSION['Name_name_product_'.$i]) && (preg_match( "#^[a-z0-9A-Z]+$#" ,$_SESSION['Name_name_product_'.$i]) == FALSE)) ){
            echo '<p class="Erreur_result_post" > ERREUR : </p>';
            $erreur = "oui";
        }
        else {
         $erreur  = "non";
        }
    }
    if ($_SESSION['Name_number_product_'.$i] < 0){
        echo '<p class="Result_post" > Le nombre de produit doit être supérieur à 0 </p>';
    }    

    if ($_SESSION['Name_price_product_'.$i] < 0){
        echo '<p class="Result_post" > Le prix du produit doit être supérieur à 0 </p>';
     }

    if (isset($_SESSION['Name_name_product_'.$i])){
        if (preg_match( "#^[a-z0-9A-Z]+$#" ,$_SESSION['Name_name_product_'.$i]) == FALSE){
            echo '<p class="Result_post" > Le nom doit contenir que des lettres et ou chiffres </p>';
        }
    }
    if(isset ($_FILES['Name_name_file']['tmp_name']) == TRUE){
        if ( !in_array($info,$extensions_valides) ){
            echo '<p class="Result_post" > Extension incorrecte </p>';
            $erreur = "oui";
        }
        if ($_FILES['Name_name_file']['size'] > 1000000){
            echo '<p class="Result_post" > Image trop lourd </p>';
            $erreur = "oui";
        }
        if ($erreur != "oui"){
           $resultat = move_uploaded_file($_FILES['Name_name_file']["tmp_name"],"Image/Photo_".$i);
        }
    }
    if ($erreur == "oui") {   
        $_SESSION['product_number']-=1;
    }
    else {
         $lecture = fopen('Dossier_CSV/fichier_csv.csv', "r");
         while (($lignes[] = fgetcsv($lecture, 1000, ";")) !== FALSE) {
         }
         $lignes[] = array($_SESSION["Name_number_product_".$i], $_SESSION["Name_price_product_".$i],$_SESSION["Name_name_product_".$i] ,"Image/Photo_".$i );
        fclose($lecture);

        $chemin = 'Dossier_CSV/fichier_csv.csv';
        $delimiteur = ';';
        $fichier_csv = fopen($chemin, 'r+');
        foreach($lignes as $ligne){
            fputcsv($fichier_csv, $ligne, $delimiteur);
        }
     
    }

    }


      ?>
  </div>


          

</body>
 </html>
