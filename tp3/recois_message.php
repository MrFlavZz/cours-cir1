<?php
session_start();

$ini_array = parse_ini_file("secrets.ini", true);

try {
   $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
   $bdd = new PDO($ini_array['db']['dsn'], $ini_array['db']['user'], $ini_array['db']['pass'], $opts);
} catch (Exception $e) {
        exit('Erreur de connexion à la base de données.'.$e->getMessage());
}

$datetime = date("Y-m-d H:i:s");
if (stripos ($_POST['Name_message'], '/ban ' ,0) === 0){
        preg_match('/\ [^\ ]*\ /', $_POST['Name_message'], $matches,PREG_OFFSET_CAPTURE); 
        $name=str_replace(' ','',$matches[0][0]);
        $cause = substr($_POST['Name_message'], 4+strlen($matches[0][0]));
        $req = $bdd->prepare('UPDATE messages SET message = :nv_message , pseudo = :nv_pseudo WHERE pseudo = :pseudonyme');
        $req->execute(array('nv_message' => "/ban Censuré par la modération parce que : $cause", 'nv_pseudo' => "NULL" , 'pseudonyme' => $name)); 
        
}
else if (stripos ($_POST['Name_message'], '/clear' ,0) === 0){
    $bdd ->exec('DELETE FROM messages');
    print("mdr");
}
else {
    $req = $bdd->prepare('INSERT INTO messages (pseudo, message,date) VALUES(?, ?, ?)');
    $req->execute(array($_SESSION['nom'], $_POST['Name_message'],$datetime));
}

header('Location: Chat.php');
